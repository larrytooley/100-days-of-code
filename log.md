# 100 Days Of Code - Log

### Day 3: January 8, 2021

**Today's Progress**: Started python feed reader project.

**Thoughts:** It was very easy to get the basics done for this. I created a plain text file with a feed list and read from the file and used requests to pull the feeds.

**Link to work:** [https://gitlab.com/larrytooley/scan-flow/-/commit/a8f0d661893a594184b0371cca39e89001582883](https://gitlab.com/larrytooley/scan-flow/-/commit/a8f0d661893a594184b0371cca39e89001582883)


### Day 2: January 7, 2021

**Today's Progress**: Completed ex14 and ex15 of Learn C the Hard Way. ex14 concerned functions and ex15 is about pointers.

**Thoughts:** I am still not sure why you would use pointers but I understand them and it is interesting and makes sense that you have to forward declare functions in C if you will not be using them in the order they are declared.

**Link to work:** [https://gitlab.com/larrytooley/learn-c-the-hard-way](https://gitlab.com/larrytooley/learn-c-the-hard-way)


### Day 1: January 6, 2021

**Today's Progress**: Completed week1 one of weekly python exercises - ISBN Checksum

**Thoughts:** I was not confident I could complete it with out looking at the solution. I created TODO's for all the steps I though I would need to complete and wrote lines of code that executed the steps. I ended up struggling with pytest, did some reading and figured out how to run the tests.

**Link to work:** [Commit with final answer](https://gitlab.com/larrytooley/weekly-python-exercise-2021-a1/-/commit/a0a3847ca6fea45977eeafb490dc8a6dd5c5605f)






<!-- ### Day 0: February 30, 2016 (Example 1)
##### (delete me or comment me out)

**Today's Progress**: Fixed CSS, worked on canvas functionality for the app.

**Thoughts:** I really struggled with CSS, but, overall, I feel like I am slowly getting better at it. Canvas is still new for me, but I managed to figure out some basic functionality.

**Link to work:** [Calculator App](http://www.example.com)

### Day 0: February 30, 2016 (Example 2)
##### (delete me or comment me out)

**Today's Progress**: Fixed CSS, worked on canvas functionality for the app.

**Thoughts**: I really struggled with CSS, but, overall, I feel like I am slowly getting better at it. Canvas is still new for me, but I managed to figure out some basic functionality.

**Link(s) to work**: [Calculator App](http://www.example.com)


### Day 1: June 27, Monday

**Today's Progress**: I've gone through many exercises on FreeCodeCamp.

**Thoughts** I've recently started coding, and it's a great feeling when I finally solve an algorithm challenge after a lot of attempts and hours spent.

**Link(s) to work**
1. [Find the Longest Word in a String](https://www.freecodecamp.com/challenges/find-the-longest-word-in-a-string)
2. [Title Case a Sentence](https://www.freecodecamp.com/challenges/title-case-a-sentence)
-->
